#include <stdio.h>
#include <stdarg.h>

float essai(int nombre, ...)
{
  int i;
  va_list ap;
  va_start(ap,nombre);
	//printf("le nombre est : %d",nombre);
	int min = va_arg(ap, int);
	if (nombre !=1){
		int res;
		for (i=1;i<nombre;i++){
		 	res = va_arg(ap, int);
			if(min>res){
				min =res;
			}
		}
	}
	va_end(ap);
 return min;
}

int main()
{
  int res = essai(4,5,3,7,9);
	printf("le res est : %d",res);
  return 0;
}
