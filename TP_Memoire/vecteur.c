#include "vecteur.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    vecteur v;
    v.taille=taille;
    v.donnees = malloc (sizeof(double)*taille);
    return v;
}

void liberer_vecteur(vecteur v) {
	free( v.donnees );
}

int est_vecteur_invalide(vecteur v) {
    int resultat = 0;
    if (v.taille<0 ||v.donnees == NULL)
    	resultat = 1;
    return resultat;
}

double *acces_vecteur(vecteur v, int i) {
	
    double *resultat = NULL;
    if (v.taille >i)
    	resultat = &v.donnees[i];
    return resultat;
}

int taille_vecteur(vecteur v) {
    int resultat = v.taille;
    return resultat;
}
