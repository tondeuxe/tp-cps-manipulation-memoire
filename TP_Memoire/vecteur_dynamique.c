#include "vecteur_dynamique.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    vecteur v = NULL;
    v= malloc(sizeof(struct donnees_vecteur));
    v->donnees = malloc(sizeof(double)*taille);
    v->taille=taille;
    return v;
}

void liberer_vecteur(vecteur v) {
		free(v->donnees);
    free(v);
}

int est_vecteur_invalide(vecteur v) {
    int resultat = 0;
    if (v==NULL||v->taille<0||(&(v->donnees)==NULL))
    	resultat = 1;

    return resultat;
}

double *acces_vecteur(vecteur v, int i) {
    double *resultat = NULL;
    if (i<0){
  		return NULL;
    }else if (i<v->taille){
    	resultat = &v->donnees[i];
    }else{
    	v->taille = v->taille*2;
    	v->donnees = realloc(v->donnees, sizeof(double)*v->taille);
    	resultat = acces_vecteur(v, i);
    }
    return resultat;
}

int taille_vecteur(vecteur v) {
    int resultat = 0;
    resultat = v->taille;
    return resultat;
}
