#include "matrice_dynamique.h"
#include "vecteur_dynamique.h"
#include <stdlib.h>

matrice allouer_matrice(int l, int c) {
    matrice m;
    m = malloc(sizeof(struct donnees_matrice));
    m->l = l;
    m->c = c;
    m->donnees = malloc(sizeof(double)*l*c);
    return m;
}

void liberer_matrice(matrice m) {
  free(m->donnees);
  free(m);
}

int est_matrice_invalide(matrice m) {
    int resultat = 0;
    if (m==NULL||m->l<0||m->c<0||(&(m->donnees)==NULL)){
       resultat = 1;
    }
    return resultat;
}

double *acces_matrice(matrice m, int i, int j) {
    double *resultat = NULL;
    if (i<0||j<0) {
      return NULL;
    } else if (m->l > i && m->c > j) {
      resultat = m->donnees[i*(m->c) + j];
    } else {
      m->l = m->l*2;
      m->c = m->c*2;
      m->donnees = realloc(m->donnees, sizeof(double)*(m->l)*(m->c));
      resultat = acces_matrice(m, i, j);
    }
    return resultat;
}

int nb_lignes_matrice(matrice m) {
    return m->l;
}

int nb_colonnes_matrice(matrice m) {
    return m->c;
}
