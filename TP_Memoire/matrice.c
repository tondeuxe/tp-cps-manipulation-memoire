#include "matrice.h"
#include <stdlib.h>

matrice allouer_matrice(int l, int c) {
    matrice m = { l, c, NULL };
    m.donnees = malloc (l*c*sizeof(double)*c);
    for (int i=0; i<l; i++){
        m.donnees[i] = malloc(sizeof(double)*c);
    }
    return m;
}

void liberer_matrice(matrice m) {
    for (int i=0; i<m.l; i++){
        free(m.donnees[i]);
    }
    free(m.donnees);
}

int est_matrice_invalide(matrice m) {
    int resultat = 0;
    if (m.l<0||m.c<0)
        resultat = 1;
    else
        for (int i=0; i<m.l; i++){
            for (int j=0; j<m.c; j++){
                if (&m.donnees[i][j]==NULL)
                    resultat = 1;
            }
        }
    return resultat;
}

double *acces_matrice(matrice m, int i, int j) {
    double *resultat = NULL;
    resultat = &m.donnees[i][j];
    return resultat;
}

int nb_lignes_matrice(matrice m) {
    int resultat = m.l;
    return resultat;
}

int nb_colonnes_matrice(matrice m) {
    int resultat = m.c;
    return resultat;
}
