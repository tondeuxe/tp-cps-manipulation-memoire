#include "memory_operations.h"
#include <stdlib.h>

void *my_memcpy(void *dst, const void *src, size_t len) {
    void * tmp = malloc (sizeof(char * )*len);
    for (int i = 0; i < len; i++) {
      *((char * )tmp +i )=*((char * )src +i );
    }
    for (int i = 0; i < len; i++) {
      *((char * )dst +i )=*((char * )tmp +i );
    }
    free(tmp);
    return dst;
}

void *my_memmove(void *dst, const void *src, size_t len) {
    //void *resultat = NULL;
    if (dst + len>=src){
      for (int i = len-1; i>=0; i--) {
        *((char * )src +i )=*((char * )dst +i );
      }
    }else{
      for (int i = 0; i < len; i++) {
        *((char * )dst +i )=*((char * )src +i );
      }
    }
    return dst;
}

int is_little_endian() {
  unsigned int i = 1;
  char *c = (char*)&i;
  if (*c)
     return 1;
 else
      return 0;

}

int reverse_endianess(int value) {
	return (int) (value & 0x000000FF) << 24 | (value & 0x0000FF00) << 8 |
			 (value & 0x00FF0000) >> 8 | (value & 0xFF000000) >> 24;
}
